# Check if the parameter are informed
if($args[0] -Eq $null -Or $args[1] -Eq ''){
	Print ('Version can t be null!', '-foregroundcolor red')
	exit
}

# Version of the csproj and Harpy packages
$version = $args[0]

# Version of Myth packages
$mythVersion = $version

# Check if the Myth Version are informed (not required)
if($args[1] -ne $null -And $args[1] -ne ''){
	$mythVersion = $args[1]
}

# Name of the current project
$name = 'Harpy'

# Projects to build (in order of dependency)
$projects = @(	
	"Harpy.Domain\Harpy.Domain.csproj",
	"Harpy.Application\Harpy.Application.csproj",
	"Harpy.Bus\Harpy.Bus.csproj",
	"Harpy.Transaction\Harpy.Transaction.csproj",
	"Harpy.Context\Harpy.Context.csproj",
	"Harpy.Repository\Harpy.Repository.csproj",
	"Harpy.IoC\Harpy.IoC.csproj",
	"Harpy.Plugin\Harpy.Plugin.csproj",
	"Harpy.Presentation\Harpy.Presentation.csproj",
	"Harpy.Test.Domain\Harpy.Test.Domain.csproj",
	"Harpy.Test.Presentation\Harpy.Test.Presentation.csproj"
)	

# Projects to ignore on delete cached packages
$ignore = @(
	'cli',
	'sqlite',
	'litedb',
	'oracle',
	'postgresql'
)

# Define the current path
$currentPath = $PSScriptRoot # Get the current script path

# Run the script
& ..\Set-Myth-Version.ps1 $version $mythVersion $name $projects $currentPath $ignore