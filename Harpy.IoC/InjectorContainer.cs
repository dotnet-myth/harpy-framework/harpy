﻿using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using Hangfire;
using Harpy.Bus;
using Harpy.Domain.Interfaces.Definitions;
using Harpy.Domain.Interfaces.Messages;
using Harpy.Domain.Interfaces.Queries.Base;
using Harpy.Domain.Interfaces.Repositories;
using Harpy.Domain.Interfaces.Repositories.Base;
using Harpy.Domain.Interfaces.Validations;
using Harpy.IoC.Extensions;
using Harpy.IoC.Options;
using Harpy.Repository;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.FeatureManagement;
using Myth.DependencyInjection;
using Myth.Extensions;
using Myth.Interfaces.Repositories.Base;
using Myth.Interfaces.Repositories.Results;
using Myth.Repositories.EntityFramework;
using Myth.Repositories.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ServiceCollectionExtensions = Myth.DependencyInjection.ServiceCollectionExtensions;

namespace Harpy.IoC {

    public static class InjectorContainer {

        public static IServiceCollection AddCustomAssemblies( this IServiceCollection services, params Assembly[ ] assemblies ) {
            if ( assemblies == null )
                ServiceCollectionExtensions.AddCustomAssemblies( assemblies );
            return services;
        }

        public static IServiceCollection AddAntiCorruptionLayer( this IServiceCollection services ) =>
            services.AddServicesFromType<IAntiCorruptionLayerRepository>( );

        public static IServiceCollection AddRepositories( this IServiceCollection services ) =>
            services.AddServicesFromType<IRepository>( );

        public static IServiceCollection AddQueries( this IServiceCollection services ) =>
            services.AddServicesFromType<IQuery>( );

        public static IServiceCollection AddValidations( this IServiceCollection services ) {
            const string fluentValidationNamespace = "FluentValidation";
            services.AddServicesFromType<IValidation>( interfaceName: nameof( IValidation ), filterNamespaces: fluentValidationNamespace );
            return services;
        }

        public static IServiceCollection AddMapper( this IServiceCollection services ) {
            services.AddAutoMapper( cfg => {
                cfg.AddExpressionMapping( );
                cfg.CreateMap( typeof( IPaginated<> ), typeof( Paginated<> ) );
                cfg.CreateMap( typeof( IPaginated<> ), typeof( IPaginated<> ) ).As( typeof( Paginated<> ) );
            }, GetAssemblies( ) );

            services
                .BuildServiceProvider( )
                .GetService<IMapper>( )
                .ConfigurationProvider
                .AssertConfigurationIsValid( );

            return services;
        }

        public static IServiceCollection AddMediatR( this IServiceCollection services ) =>
            services.AddMediatR( x => x.AsTransient( ), GetAssemblies( ).ToArray( ) );

        public static IServiceCollection AddHangfire( this IServiceCollection services, HarpySettings settings = null ) {
            if ( settings.JobStorage is null )
                throw new Exception( "Job Storage not set!" );

            return services.AddHangfire( settings.JobStorage );
        }

        private static IServiceCollection AddFeatureManagement( this IServiceCollection services, HarpySettings harpySettings ) {
            var serviceProvider = services.BuildServiceProvider( );
            var configuration = serviceProvider.GetService<IConfiguration>( );

            services.AddFeatureManagement( configuration.GetSection( harpySettings.FeatureSection ) );
            return services;
        }

        private static IServiceCollection AddPropertyInjections( this IServiceCollection services ) {
            var serviceProvider = services.BuildServiceProvider( );

            OdataExtension.Configure( serviceProvider.GetService<IMapper>( ) );
            AutoMapperExtension.Configure( serviceProvider.GetService<IMapper>( ) );
            BusExtension.Configure( serviceProvider.GetService<IMessageBus>( ) );
            FeatureManagmentExtension.Configure( serviceProvider.GetService<IFeatureManager>( ) );

            return services;
        }

        private static IServiceCollection AddEventRepository( this IServiceCollection services ) {
            // Add EventRepository only if a database context was added
            if ( services.Any( x => x.ServiceType == typeof( HarpyDbOptions ) ) )
                services.AddTransient<IEventRepository, EventRepository>( );

            return services;
        }

        private static IServiceCollection AddMessageBus( this IServiceCollection services, HarpySettings harpySettings ) {
            switch ( harpySettings.UseMessageBus ) {
                case HarpySettings.MessageBusType.Memory:
                default:
                    services.AddScoped<IMessageBus, MemoryBus>( );
                    break;
            }

            return services;
        }

        public static string RaiseInBackground( this IServiceCollection services, IJob job ) =>
            services.BuildServiceProvider( ).GetService<IMessageBus>( ).RaiseJob( job );

        public static TQuery GetQuery<TQuery>( this IServiceCollection services ) where TQuery : IQuery =>
            services.BuildServiceProvider( ).GetService<TQuery>( );

        public static IEnumerable<Assembly> GetAssemblies( ) => ServiceCollectionExtensions.GetAssemblies( );

        public static IEnumerable<Type> GetTypes( ) => ServiceCollectionExtensions.GetTypes( );

        public static IServiceCollection AddHarpy( this IServiceCollection services, Action<HarpySettings> settings = null, params Assembly[ ] assemblies ) {
            var harpySettings = new HarpySettings( );

            if ( settings is not null )
                settings.Invoke( harpySettings );

            var logger = services
                .BuildServiceProvider( )
                .GetService<ILoggerFactory>( )
                .CreateLogger( "Harpy.IoC.Containers" );

            logger.LogInformation( "Adding validations..." );
            services.AddValidations( );

            logger.LogInformation( "Adding event repsitory..." );
            services.AddHttpContextAccessor( );
            services.AddEventRepository( );

            logger.LogInformation( "Adding custom assemblies..." );
            services.AddCustomAssemblies( assemblies );

            logger.LogInformation( "Adding anti-corruption layers..." );
            services.AddAntiCorruptionLayer( );

            logger.LogInformation( "Adding repositories..." );
            services.AddRepositories( );

            logger.LogInformation( "Adding queries..." );
            services.AddQueries( );

            logger.LogInformation( "Adding libraries..." );
            services.AddMapper( );
            services.AddMediatR( );
            services.AddHangfire( harpySettings );

            logger.LogInformation( "Adding message bus..." );
            services.AddMessageBus( harpySettings );

            logger.LogInformation( "Adding feature toogle..." );
            services.AddFeatureManagement( harpySettings );

            logger.LogInformation( "Configuring extensions..." );
            services.AddPropertyInjections( );

            return services;
        }
    }
}