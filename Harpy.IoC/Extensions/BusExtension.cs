﻿using Hangfire;
using Harpy.Domain.Interfaces.Definitions;
using Harpy.Domain.Interfaces.Messages;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.IoC.Extensions {

    public static class BusExtension {
        private static IMessageBus _bus;

        public static void Configure( IMessageBus bus ) {
            _bus = bus;
        }

        public static Task<TResponse> SendAsync<TResponse>( this ICommand<TResponse> command, CancellationToken cancellationToken = default ) {
            return _bus.SendCommandAsync<TResponse>( command, cancellationToken );
        }

        public static string Raise( this IJob job, IJobCancellationToken cancellationToken = default ) {
            return _bus.RaiseJob( job, cancellationToken );
        }

        public static Task RaiseAsync( this IEvent @event, CancellationToken cancellationToken = default ) {
            return _bus.RaiseEventAsync( @event, cancellationToken );
        }
    }
}