﻿using Hangfire;
using Harpy.Domain.Attributes.Jobs;
using Harpy.IoC.Options.Jobs.Activators;
using Microsoft.Extensions.DependencyInjection;

namespace Harpy.IoC.Extensions {

    public static class HangfireExtension {

        public static IServiceCollection AddHangfire<TStorage>( this IServiceCollection services, TStorage storage ) where TStorage : JobStorage {
            services.AddHangfire( configuration => {
                configuration
                    .SetDataCompatibilityLevel( CompatibilityLevel.Version_170 )
                    .UseSimpleAssemblyNameTypeSerializer( )
                    .UseRecommendedSerializerSettings( )
                    .UseStorage( storage )
                    .UseFilter( new PostponeJobAttribute( ) );

                configuration.UseActivator( new HarpyJobActivator( services ) );
                JobActivator.Current = new HarpyJobActivator( services );
            } );

            GlobalJobFilters.Filters.Add( new PostponeJobAttribute( ) );
            JobStorage.Current = storage;

            services.AddHangfireServer( options => {
                options.ServerName = "HarpyJobs";
                options.WorkerCount = 1;
                options.FilterProvider = GlobalJobFilters.Filters;
            } );

            return services;
        }
    }
}