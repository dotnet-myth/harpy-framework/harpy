﻿using Microsoft.FeatureManagement;
using System.Threading.Tasks;

namespace Harpy.IoC.Extensions {

    public static class FeatureManagmentExtension {
        private static IFeatureManager _featureManager;

        public static void Configure( IFeatureManager featureManager ) {
            _featureManager = featureManager;
        }

        public static async Task<bool> IsEnabledAsync( string feature ) =>
            _featureManager is null || await _featureManager.IsEnabledAsync( feature );
    }
}