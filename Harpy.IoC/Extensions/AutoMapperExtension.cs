﻿using AutoMapper;
using System.Threading.Tasks;

namespace Harpy.IoC.Extensions {

    public static class AutoMapperExtension {
        private static IMapper _mapper;

        public static void Configure( IMapper mapper ) {
            _mapper = mapper;
        }

        public static TSource MapTo<TSource>( this object model ) => _mapper.Map<TSource>( model );

        public static async Task<TDest> MapToAsync<TSource, TDest>( this Task<TSource> task ) => _mapper.Map<TDest>( await task );

        public static async ValueTask<TDest> MapToAsync<TSource, TDest>( this ValueTask<TSource> task ) => _mapper.Map<TDest>( await task );
    }
}