﻿using FluentMigrator;

namespace Harpy.IoC.Migrations {

    [Migration( 1L, "Add table for internal harpy events" )]
    public class _20201209_AddTableEventLog : Migration {

        public override void Up( ) {
            Create
                .Table( "event_log" )
                .WithColumn( "event_log_id" ).AsGuid( ).PrimaryKey( )
                .WithColumn( "content" ).AsString( ).NotNullable( )
                .WithColumn( "created_at" ).AsDateTime( ).NotNullable( )
                .WithColumn( "type_name" ).AsString( ).NotNullable( )
                .WithColumn( "state" ).AsInt32( ).NotNullable( );
        }

        public override void Down( ) {
            Delete.Table( "event_log" );
        }
    }
}