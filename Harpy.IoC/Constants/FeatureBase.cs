﻿using Harpy.IoC.Extensions;
using Myth.ValueObjects;
using System.Threading.Tasks;

namespace Harpy.IoC.Constants {

    public abstract class FeatureBase : Constant<FeatureBase, string> {

        public FeatureBase( string name ) : base( name, name ) {
        }

        public virtual async Task<bool> IsEnabledAsync( ) => await FeatureManagmentExtension.IsEnabledAsync( Name );
    }
}