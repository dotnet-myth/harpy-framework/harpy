﻿using FluentMigrator.Runner;
using Harpy.Context;
using Harpy.IoC.Migrations;
using Harpy.IoC.Options;
using Harpy.IoC.Options.Jobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Myth.Contexts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Harpy.IoC {

    public static class DatabaseContainer {
        private static IServiceCollection _services;

        private static JobContextFactory JobContextFactory;

        public static IServiceCollection AddDatabase( this IServiceCollection services, Func<DbContextOptionsBuilder, IDbConnection> UseConnection, Action<HarpyDbOptions> AddContexts, Action<DbContextOptionsBuilder> options = null ) {
            _services = services;
            JobContextFactory = new JobContextFactory( );

            var dbContext = new DbContextOptionsBuilder( )
                .UseLazyLoadingProxies( );

            var jobDbContext = new DbContextOptionsBuilder( )
                .UseLazyLoadingProxies( );

            var connection = UseConnection.Invoke( dbContext );
            UseConnection.Invoke( jobDbContext );

            services.AddScoped<IDbConnection>( ( serviceProvider ) => {
                if ( connection.State == ConnectionState.Closed )
                    connection.Open( );

                return connection;
            } );

            if ( options != null )
                options.Invoke( dbContext );

            jobDbContext.EnableLog( );

            var optionsContext = new HarpyDbOptions( dbContext.Options, jobDbContext.Options );

            services.AddScoped<HarpyDbOptions>( ( serviceProvider ) => optionsContext );

            optionsContext.AddContext<EventStoreContext>( );

            AddContexts.Invoke( optionsContext );

            services.AddScoped( x => optionsContext.DeafultContextOptions );

            services.AddScoped( x => JobContextFactory );

            _services = null;

            return services;
        }

        public static HarpyDbOptions AddContext<TContext>( this HarpyDbOptions dbContextOption, Action<Parameters> contextParams = null ) where TContext : BaseContext {
            _services.AddScoped( ( serviceProvider ) => {
                var args = new Parameters( );
                if ( contextParams != null )
                    contextParams.Invoke( args );

                var optionsList = serviceProvider.GetService<IEnumerable<HarpyDbOptions>>( );

                var options = optionsList.FirstOrDefault( x => x.Guid == dbContextOption.Guid );

                var context = ( TContext )Activator.CreateInstance( typeof( TContext ), options.DeafultContextOptions, args );

                JobContextFactory.Add( ( TContext )Activator.CreateInstance( typeof( TContext ), options.JobContextOptions, args ) );

                var transaction = serviceProvider.GetService<DbTransaction>( );
                if ( transaction != null )
                    context.Database.UseTransaction( transaction );

                return context;
            } );
            return dbContextOption;
        }

        public static Parameters AddParameter( this Parameters parameters, string key, object value ) {
            parameters.Add( key, value );
            return parameters;
        }

        public static DbContextOptionsBuilder EnableLog( this DbContextOptionsBuilder dbContextOptionsBuilder ) {
            var loggerFactory = _services.BuildServiceProvider( ).GetService<ILoggerFactory>( );

            dbContextOptionsBuilder
                .UseLoggerFactory( loggerFactory )
                .EnableSensitiveDataLogging( )
                .EnableDetailedErrors( );

            return dbContextOptionsBuilder;
        }

        public static DbContextOptionsBuilder EnableMigrations( this DbContextOptionsBuilder dbContextOptionsBuilder, Action<IMigrationRunner> migrationOptions = null, bool showLog = true ) {
            var optionsExtension = ( dbContextOptionsBuilder.Options.Extensions.FirstOrDefault( x => x.Info.IsDatabaseProvider ) as RelationalOptionsExtension );
            var connection = optionsExtension.Connection;
            var migrationName = optionsExtension.MigrationsAssembly;

            var migrationFile = new DirectoryInfo( Directory.GetCurrentDirectory( ) )
                .GetFiles( "*.dll", SearchOption.AllDirectories )
                .FirstOrDefault( x => x.Name.Contains( migrationName ) );

            var migrationAssembly = Assembly.LoadFile( migrationFile.FullName );
            var harpyMigrations = typeof( _20201209_AddTableEventLog ).Assembly;

            _services
                .AddFluentMigratorCore( )
                .ConfigureRunner( runnerBuilder => {
                    runnerBuilder
                        .WithVersionTable( MigrationVersionTable.Default )
                        .WithGlobalConnectionString( connection.ConnectionString )
                        .ScanIn( harpyMigrations, migrationAssembly ).For.Migrations( );

                    switch ( connection.GetType( ).Name ) {
                        case "SqliteConnection":
                            runnerBuilder.AddSQLite( );
                            break;

                        case "OracleConnection":
                            runnerBuilder.AddOracle( );
                            break;

                        case "NpgsqlConnection":
                            runnerBuilder.AddPostgres( );
                            break;

                        case "MySqlConnection":
                            runnerBuilder.AddMySql5( );
                            break;

                        case "SqlConnection":
                            runnerBuilder.AddSqlServer( );
                            break;

                        default:
                            throw new Exception( "Can't identify database type!" );
                    };
                } );

            var serviceProvider = _services.BuildServiceProvider( );

            if ( showLog ) {
                var loggerFactory = serviceProvider.GetService<ILoggerFactory>( );

                loggerFactory.AddFluentMigratorConsole( );

                _services
                    .Configure<FluentMigratorLoggerOptions>( cfg => {
                        cfg.ShowElapsedTime = true;
                        cfg.ShowSql = true;
                    } );
            }

            if ( migrationOptions != null ) {
                var migrationRunner = serviceProvider.GetService<IMigrationRunner>( );
                migrationOptions.Invoke( migrationRunner );
            }

            return dbContextOptionsBuilder;
        }

        public static IMigrationRunner EnsureAllMigrationsApplied( this IMigrationRunner migrationRunner ) {
            if ( migrationRunner.HasMigrationsToApplyUp( ) ) {
                migrationRunner.ListMigrations( );
                migrationRunner.MigrateUp( );
            }

            return migrationRunner;
        }

        public static IMigrationRunner EnsureMigrationsUntilApplied( this IMigrationRunner migrationRunner, long version ) {
            if ( migrationRunner.HasMigrationsToApplyUp( version ) ) {
                migrationRunner.ListMigrations( );
                migrationRunner.MigrateUp( version );
            }

            return migrationRunner;
        }
    }
}