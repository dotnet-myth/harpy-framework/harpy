﻿using Hangfire;

namespace Harpy.IoC.Options {

    public class HarpySettings {
        public JobStorage JobStorage { get; set; }

        public MessageBusType UseMessageBus { get; set; }

        public string FeatureSection { get; set; }

        public HarpySettings( ) {
            UseMessageBus = MessageBusType.Memory;
            FeatureSection = "FeatureManagement";
        }

        public enum MessageBusType {
            Memory,
        }
    }
}