﻿using System.Collections.Generic;

namespace Harpy.IoC.Options {

    public class Parameters {
        private Dictionary<string, object> Parameter { get; set; }

        public Parameters( ) {
            Parameter = new Dictionary<string, object>( );
        }

        public Parameters( params (string, object)[ ] args ) : this( ) {
            for ( int i = 0; i < args.Length; i++ )
                Add( args[ i ].Item1, args[ i ].Item2 );
        }

        public void Add( string key, object value ) {
            Parameter.Add( key, value );
        }

        public T Get<T>( string key ) {
            return ( T )Parameter[ key ];
        }

        public int Count( ) {
            return Parameter.Count;
        }
    }
}