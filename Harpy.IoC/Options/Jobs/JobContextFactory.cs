﻿using Myth.Contexts;
using System;
using System.Collections.Generic;

namespace Harpy.IoC.Options.Jobs {

    public class JobContextFactory {
        public IDictionary<Type, BaseContext> Contexts { get; private set; }

        public JobContextFactory( ) {
            Contexts = new Dictionary<Type, BaseContext>( );
        }

        public void Add<T>( T context ) where T : BaseContext {
            if ( !Contexts.ContainsKey( context.GetType( ) ) )
                Contexts.Add( context.GetType( ), context );
        }

        public T Get<T>( ) where T : BaseContext {
            return Contexts[ typeof( T ) ] as T;
        }

        public object Get( Type type ) {
            return Contexts[ type ];
        }
    }
}