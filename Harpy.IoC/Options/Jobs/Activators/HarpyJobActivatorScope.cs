﻿using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Harpy.IoC.Options.Jobs.Activators {

    internal class HarpyJobActivatorScope : JobActivatorScope {
        private IServiceProvider _serviceProvider;

        public HarpyJobActivatorScope( IServiceProvider serviceProvider ) {
            _serviceProvider = serviceProvider;
        }

        public override object Resolve( Type type ) =>
            ActivatorUtilities.GetServiceOrCreateInstance( _serviceProvider, type );
    }
}