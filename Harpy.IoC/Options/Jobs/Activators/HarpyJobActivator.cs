﻿using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using Myth.Contexts;

namespace Harpy.IoC.Options.Jobs.Activators {

    public class HarpyJobActivator : JobActivator {
        private readonly IServiceCollection _services;

        public HarpyJobActivator( IServiceCollection services ) {
            _services = new ServiceCollection( );
            var factory = services.BuildServiceProvider( ).GetRequiredService<JobContextFactory>( );
            foreach ( var service in services ) {
                if ( service.ServiceType.BaseType == typeof( BaseContext ) )
                    _services.AddScoped( service.ServiceType, ( serviceProvider ) => factory.Get( service.ServiceType ) );
                else
                    _services.Add( service );
            }
        }

        public override JobActivatorScope BeginScope( JobActivatorContext context ) =>
            new HarpyJobActivatorScope( _services.BuildServiceProvider( ) );
    }
}