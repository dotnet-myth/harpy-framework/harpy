﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Harpy.IoC.Options {

    public class HarpyDbOptions {
        public DbContextOptions DeafultContextOptions { get; private set; }

        public DbContextOptions JobContextOptions { get; private set; }

        public Guid Guid { get; private set; }

        protected HarpyDbOptions( ) {
            Guid = Guid.NewGuid( );
        }

        public HarpyDbOptions( DbContextOptions contextOptions ) : this( ) {
            DeafultContextOptions = contextOptions;
        }

        public HarpyDbOptions( DbContextOptions deafultContextOptions, DbContextOptions jobContextOptions ) : this( ) {
            DeafultContextOptions = deafultContextOptions;
            JobContextOptions = jobContextOptions;
        }
    }
}