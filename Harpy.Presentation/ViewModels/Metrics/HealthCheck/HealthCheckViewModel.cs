﻿using System.Collections.Generic;
using System.Linq;

namespace Harpy.Presentation.ViewModels.Metrics.HealthCheck {

    public class HealthCheckViewModel {
        public string Status { get; set; }
        public string TotalDuration { get; set; }
        public IEnumerable<HealthCheckItemViewModel> Entries { get; set; } = Enumerable.Empty<HealthCheckItemViewModel>( );
    }
}