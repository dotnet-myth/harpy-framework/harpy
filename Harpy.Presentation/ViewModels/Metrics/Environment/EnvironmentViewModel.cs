﻿namespace Harpy.Presentation.ViewModels.Metrics.Environment {

    public class EnvironmentViewModel {
        public string EnvironmentName { get; set; }
        public bool IsDevelopment { get; set; }
        public bool IsStaging { get; set; }
        public bool IsProduction { get; set; }
    }
}