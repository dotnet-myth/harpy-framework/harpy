﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace Harpy.Presentation.Attributes {

    [AttributeUsage( AttributeTargets.Parameter, Inherited = true, AllowMultiple = false )]
    public class CommaSeparatedAttribute : FromQueryAttribute {
    }
}