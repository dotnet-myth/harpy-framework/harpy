﻿using Microsoft.AspNetCore.Mvc.Filters;
using Myth.Extensions;

namespace Harpy.Presentation.Conventions {

    public class HarpyVersionHeaderFilter : ActionFilterAttribute {

        public override void OnActionExecuted( ActionExecutedContext context ) {
            context.HttpContext.Response.Headers.Add( "X-Harpy-Version", AppVersion.GetCurrent( ) );
            base.OnActionExecuted( context );
        }
    }
}