﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using Myth.Rest;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Presentation.ValueProviders.HealthChecks {

    public class InternetAccessCheck : IHealthCheck {

        public async Task<HealthCheckResult> CheckHealthAsync( HealthCheckContext context, CancellationToken cancellationToken = default ) {
            var response = await new RestBuilder( )
                .DoGet( "https://www.google.com/", cancellationToken )
                .When( when => when
                    .NonSuccessStatusCodeThrowsException( false ) )
                .BuildResultAsync( );

            var elapsedTime = response.ElapsedTime;
            var successStatusCode = response.IsSuccessStatusCode( );

            if ( successStatusCode && elapsedTime < TimeSpan.FromSeconds( 10 ) )
                return HealthCheckResult.Healthy( );

            if ( successStatusCode && elapsedTime > TimeSpan.FromSeconds( 10 ) )
                return HealthCheckResult.Unhealthy( );

            return HealthCheckResult.Unhealthy( );
        }
    }
}