﻿using HybridModelBinding;
using Myth.Extensions;
using NJsonSchema;
using NSwag;
using NSwag.Generation.AspNetCore;
using NSwag.Generation.Processors;
using NSwag.Generation.Processors.Contexts;
using System.Linq;

namespace Harpy.Presentation.ValueProviders.SwaggerFilters {

    public class HybridOperationFilter : IOperationProcessor {

        public bool Process( OperationProcessorContext context ) {
            if ( context is AspNetCoreOperationProcessorContext aspnetContext ) {
                var apiDescription = aspnetContext.ApiDescription;

                var hybridParameters = apiDescription.ParameterDescriptions
                    .Where( x => x.Source.Id == "Hybrid" )
                    .ToList( );

                var operation = context.OperationDescription.Operation;

                foreach ( var hybridParam in hybridParameters ) {
                    var defaultParameter = operation.Parameters.First( x => x.Name == hybridParam.Name );
                    operation.Parameters.Remove( defaultParameter );

                    var parameters = hybridParam.Type.GetProperties( )
                        .Where( x => x.CustomAttributes.Any( x => x.AttributeType == typeof( HybridBindPropertyAttribute ) ) )
                        .Select( x => new { Attribute = x.CustomAttributes.FirstOrDefault( x => x.AttributeType == typeof( HybridBindPropertyAttribute ) ), Type = x.PropertyType } )
                        .Select( x => {
                            var typeScheme = JsonSchema.FromType( x.Type, context.Settings );
                            return new {
                                Name = x.Attribute.ConstructorArguments[ 1 ].Value.ToString( ),
                                Kind = MapParameter( x.Attribute.ConstructorArguments[ 0 ].Value.ToString( ) ),
                                Type = typeScheme.Type,
                                RealType = x.Type,
                                JsonScheme = typeScheme
                            };
                        } )
                        .OrderByDescending( x => x.Kind )
                        .ToList( );

                    foreach ( var param in parameters ) {
                        var paramDefined = operation.Parameters.FirstOrDefault( x => x.Name.ToLower( ) == param.Name.ToLower( ) );
                        if ( paramDefined != null )
                            operation.Parameters.Remove( paramDefined );

                        if ( param.Kind != OpenApiParameterKind.Body ) {
                            var propScheme = context.Document.Definitions[ hybridParam.Type.Name ].Properties[ param.Name.ToCamelCase( ) ];
                            operation.Parameters.Add(
                                new OpenApiParameter {
                                    Name = param.Name.ToCamelCase( ),
                                    Type = param.Type,
                                    Kind = param.Kind,
                                    IsRequired = true,
                                    Schema = propScheme,
                                } );
                            context.Document.Definitions[ hybridParam.Type.Name ].Properties.Remove( param.Name.ToCamelCase( ) );
                        } else {
                            var body = operation.Parameters.FirstOrDefault( x => x.Kind == OpenApiParameterKind.Body );
                            if ( body == null ) {
                                body = new OpenApiParameter {
                                    Name = defaultParameter.Name.ToCamelCase( ),
                                    Kind = OpenApiParameterKind.Body,
                                    Type = JsonObjectType.Object,
                                    IsRequired = true,
                                    Schema = context.Document.Definitions[ hybridParam.Type.Name ]
                                };
                                operation.Parameters.Add( body );
                            }
                            body.Properties.Add( param.Name.ToCamelCase( ), new JsonSchemaProperty { Type = param.Type } );
                        }
                    }
                }
            }
            return true;
        }

        private OpenApiParameterKind MapParameter( string type ) {
            return type switch {
                "Body" => OpenApiParameterKind.Body,
                "Form" => OpenApiParameterKind.FormData,
                "Header" => OpenApiParameterKind.Header,
                "QueryString" => OpenApiParameterKind.Query,
                "Route" => OpenApiParameterKind.Path,
                _ => OpenApiParameterKind.Undefined,
            };
        }
    }
}