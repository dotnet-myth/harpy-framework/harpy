﻿using Harpy.IoC.Extensions;
using Microsoft.FeatureManagement.Mvc;
using NSwag.Generation.Processors;
using NSwag.Generation.Processors.Contexts;
using System.Linq;

namespace Harpy.Presentation.ValueProviders.SwaggerFilters {

    public class FeatureManagerFilter : IOperationProcessor {

        public bool Process( OperationProcessorContext context ) {
            var featureAttributes = context.MethodInfo
                .GetCustomAttributes( typeof( FeatureGateAttribute ), true )
                .Select( x => x as FeatureGateAttribute )
                .ToList( );

            if ( !featureAttributes.All( x => x.Features.All( y => FeatureManagmentExtension.IsEnabledAsync( y ).Result ) ) )
                return false;

            return true;
        }
    }
}