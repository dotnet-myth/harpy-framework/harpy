﻿using Microsoft.AspNetCore.Builder;
using System.Web;

namespace Harpy.Presentation.Extensions {

    public static class HeaderExtension {

        public static IApplicationBuilder UseAuthorizationMiddleware( this IApplicationBuilder app ) {
            app.Use( async ( context, next ) => {
                if ( context.Request.QueryString.HasValue ) {
                    if ( string.IsNullOrWhiteSpace( context.Request.Headers[ "Authorization" ] ) ) {
                        var querystring = HttpUtility.ParseQueryString( context.Request.QueryString.Value );
                        var token = querystring.Get( "access_token" );
                        if ( !string.IsNullOrWhiteSpace( token ) )
                            context.Request.Headers.Add( "Authorization", new[ ] { string.Format( "Bearer {0}", token ) } );
                    }
                }
                await next.Invoke( );
            } );
            return app;
        }
    }
}