﻿using GlobalExceptionHandler.WebApi;
using Harpy.Domain.Exceptions;
using Harpy.Domain.Models.Errors;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Harpy.Presentation.Extensions {

    public static class ExceptionConfigurationExtension {

        public static void UseExceptionMiddleware( this IApplicationBuilder app, Action<ExceptionHandlerConfiguration> exceptionsConfigurations = null ) {
            app.UseGlobalExceptionHandler( configuration => {
                configuration.ContentType = "application/json";

                configuration.ValidationError( );

                if ( exceptionsConfigurations != null )
                    exceptionsConfigurations.Invoke( configuration );

                var environment = app.ApplicationServices.GetRequiredService<IHostEnvironment>( );
                var logger = app.ApplicationServices
                    .GetRequiredService<ILoggerFactory>( )
                    .CreateLogger( "Harpy.Presentation.ExceptionManagment" );

                configuration.InternalErrorSettings( logger, environment );
            } );
        }

        private static void ValidationError( this ExceptionHandlerConfiguration configuration ) {
            configuration.Map<ValidationException>( )
                .ToStatusCode( ex => ex.StatusCode )
                .WithBody( ( ex, context ) => {
                    var exception = new ValidationResponse( ex.Errors );
                    return JsonConvert.SerializeObject( exception );
                } );
        }

        private static void InternalErrorSettings( this ExceptionHandlerConfiguration configuration, ILogger logger, IHostEnvironment environment ) {
            configuration.ResponseBody( error => {
                var exception = new ExeceptionResponse( "An internal error has occurred." );

                if ( !environment.IsProduction( ) )
                    exception = new ExeceptionResponse( error.Message, error.StackTrace, error.InnerException?.Message );

                return JsonConvert.SerializeObject( exception );
            } );

            configuration.OnError( ( exception, httpContext ) => {
                var error = new ExeceptionResponse( exception.Message, exception.StackTrace, exception.InnerException?.Message );
                logger.LogError( error.ToString( ) );
                return Task.CompletedTask;
            } );
        }
    }
}