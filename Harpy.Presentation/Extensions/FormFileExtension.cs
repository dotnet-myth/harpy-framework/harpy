﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Harpy.Presentation.Extensions {

    public static class FormFileExtensions {

        public static async Task<string> ToBase64( this IFormFile formFile ) {
            using var memoryStream = new MemoryStream( );
            await formFile.CopyToAsync( memoryStream );
            var array = memoryStream.ToArray( );
            return Convert.ToBase64String( array );
        }
    }
}