﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using System;

namespace Harpy.Presentation.Extensions {

    public static class LoggerExtension {

        public static IWebHostBuilder UseFullLog( this IWebHostBuilder builder, Action<ILoggingBuilder> logginBuilder = null ) {
            builder.ConfigureLogging( ( hostingContext, logging ) => {
                logging.ClearProviders( );
                logging.AddConsole( );

                if ( logginBuilder is not null )
                    logginBuilder.Invoke( logging );

                logging.AddFilter( "Hangfire", LogLevel.Error );
                logging.AddFilter( "Microsoft.AspNetCore.Diagnostics.ExceptionHandlerMiddleware", LogLevel.None );
                logging.AddFilter( DbLoggerCategory.Database.Command.Name, LogLevel.Information );
            } );

            return builder;
        }
    }
}