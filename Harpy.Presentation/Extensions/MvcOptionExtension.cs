﻿using Harpy.Presentation.Conventions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace Harpy.Presentation.Extensions {

    public static class MvcOptionExtension {

        public static MvcOptions AddHarpyOptions( this MvcOptions options, Action<FilterCollection> filters = null, Action<IList<IApplicationModelConvention>> conventions = null ) {
            options.Conventions.Add( new CommaSeparatedRouteConvention( ) );
            if ( conventions != null )
                conventions.Invoke( options.Conventions );

            options.Filters.Add( new HarpyVersionHeaderFilter( ) );
            if ( filters != null )
                filters.Invoke( options.Filters );
            return options;
        }
    }
}