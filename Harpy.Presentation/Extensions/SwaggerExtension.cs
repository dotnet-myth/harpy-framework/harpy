﻿using Harpy.Presentation.ValueProviders.SwaggerFilters;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NSwag;
using NSwag.Generation;
using NSwag.Generation.Processors.Collections;
using NSwag.Generation.Processors.Security;
using System;
using System.Linq;

namespace Harpy.Presentation.Extensions {

    public static class SwaggerExtension {

        public static void AddSwaggerMiddleware( this IServiceCollection services, string version, Action<SwaggerInfo> info = null, Action<OperationProcessorCollection> operationsFilters = null, Action<DocumentProcessorCollection> documentFilters = null, Action<OpenApiDocumentGeneratorSettings> settings = null ) {
            services.AddSwaggerDocument( config => {
                var swaggerSettings = new SwaggerInfo( );
                if ( info != null )
                    info.Invoke( swaggerSettings );

                config.DocumentName = $"V{version}";
                config.PostProcess = document => {
                    document.Info.Version = version;
                    document.Info.Title = swaggerSettings.Title;
                    document.Info.Description = swaggerSettings.Description;
                };

                config.OperationProcessors.Add( new HybridOperationFilter( ) );
                config.OperationProcessors.Add( new FeatureManagerFilter( ) );

                if ( operationsFilters != null )
                    operationsFilters.Invoke( config.OperationProcessors );

                if ( documentFilters != null )
                    documentFilters.Invoke( config.DocumentProcessors );

                if ( settings != null )
                    settings.Invoke( config );
            } );
        }

        public static void AddJWTSecurity( this OpenApiDocumentGeneratorSettings settings ) {
            settings.AddSecurity( "JWT", Enumerable.Empty<string>( ), new OpenApiSecurityScheme {
                Type = OpenApiSecuritySchemeType.ApiKey,
                Name = "Authorization",
                In = OpenApiSecurityApiKeyLocation.Header,
                Description = "Type into the textbox: Bearer {your JWT token}."
            } );

            settings.OperationProcessors.Add( new AspNetCoreOperationSecurityScopeProcessor( "JWT" ) );
        }

        public static void UseHarpySwagger( this IApplicationBuilder app ) {
            app.UseOpenApi( )
               .UseSwaggerUi3( )
               .UseReDoc( x => x.Path = "/ReDoc" );
        }
    }

    public class SwaggerInfo {
        public string Title { get; set; } = "Services";

        public string Description { get; set; } = "Exposed API services";
    }
}