﻿using Microsoft.Extensions.Configuration;
using Myth.Extensions;
using System;
using System.IO;

namespace Harpy.Presentation.Extensions {

    public static class ConfigurationExtension {

        public static string TreatPaths( this string value ) {
            return Environment.ExpandEnvironmentVariables( value );
        }

        public static string GetPath( this IConfiguration configuration, string key ) {
            var path = configuration.GetValue<string>( key ).TreatPaths( );
            EnsureDirectoryExists( path );
            return path;
        }

        public static string GetConnectionStringFile( this IConfiguration configuration, string name ) {
            var path = configuration.GetConnectionString( name ).TreatPaths( );
            EnsureDirectoryExists( path );
            return path;
        }

        private static void EnsureDirectoryExists( string path ) {
            var directory = new DirectoryInfo( Path.GetDirectoryName( path.GetStringBetween( '=', ';' ) ) );
            if ( !directory.Exists )
                directory.Create( );
        }
    }
}