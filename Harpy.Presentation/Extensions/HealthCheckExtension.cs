﻿using Harpy.Presentation.ValueProviders.HealthChecks;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Harpy.Presentation.Extensions {

    public static class HealthCheckExtension {

        public static IServiceCollection AddHealthCheck( this IServiceCollection services, string jobsConnectionStrig = null, Action<IHealthChecksBuilder> healthCheckBuilderSettings = null ) {
            var healthCheckBuilder = services.AddHealthChecks( )
                .AddCheck<InternetAccessCheck>( "Internet access", tags: new[ ] { "internet" } );

            if ( !string.IsNullOrEmpty( jobsConnectionStrig ) )
                healthCheckBuilder.AddSqlite( jobsConnectionStrig, name: "Jobs database", tags: new[ ] { "database" } );

            if ( healthCheckBuilderSettings is not null )
                healthCheckBuilderSettings.Invoke( healthCheckBuilder );

            return services;
        }
    }
}