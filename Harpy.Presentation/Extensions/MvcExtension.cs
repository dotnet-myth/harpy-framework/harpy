﻿using Harpy.Domain.Interfaces.Plugins;
using Harpy.IoC;
using Harpy.Presentation.Controller;
using Harpy.Presentation.ValueProviders.FeaturesController;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.FeatureManagement;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Harpy.Presentation.Extensions {

    public static class MvcExtension {

        public static IMvcBuilder AddHarpySettings( this IMvcBuilder builder ) {
            var controllers = builder.Services.BuildServiceProvider( ).GetService<IPluginControllers>( );

            builder
                .AddHybridModelBinder( )
                .AddNewtonsoftJson( options => {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver( );
                    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                } );

            if ( controllers?.Any( ) != null )
                builder
                    .ConfigureApplicationPartManager( apm => controllers
                        .ToList( )
                        .ForEach( controller => apm.ApplicationParts.Add( new AssemblyPart( controller ) ) ) )
                    .AddControllersAsServices( );

            return builder;
        }

        public static IMvcBuilder AddEndpoints( this IServiceCollection services, Action<MvcOptions> mvcOptions = null, Action<FilterCollection> filters = null, Action<IList<IApplicationModelConvention>> conventions = null ) {
            Action<MvcOptions> options = ( option ) => {
                option.AddHarpyOptions( filters, conventions );
                if ( mvcOptions != null )
                    mvcOptions.Invoke( option );
            };

            var mvcBuilder = services.AddControllers( options );

            foreach ( var type in InjectorContainer.GetTypes( ) )
                if ( type.IsSubclassOf( typeof( ApiController ) ) )
                    mvcBuilder.AddApplicationPart( type.Assembly );

            var featureManager = services.BuildServiceProvider( ).GetRequiredService<IFeatureManager>( );

            mvcBuilder.ConfigureApplicationPartManager( apm =>
                apm.FeatureProviders.Add(
                    new ControllerFeatureProvider( featureManager ) ) );

            return mvcBuilder.AddHarpySettings( );
        }
    }
}