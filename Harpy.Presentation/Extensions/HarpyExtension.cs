﻿using Hangfire;
using Microsoft.AspNetCore.Builder;

namespace Harpy.Presentation.Extensions {

    public static class HarpyExtension {

        public static IApplicationBuilder UseHarpy( this IApplicationBuilder app ) {
            app.UseHarpySwagger( );

            app.UseHangfireDashboard( );

            app.UseAuthorizationMiddleware( );

            return app;
        }
    }
}