﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;
using System;
using System.IO;

namespace Harpy.Presentation.Extensions {

    public static class StaticFileExtension {

        public static IApplicationBuilder UseStaticFiles( this IApplicationBuilder app, Action<IApplicationBuilder> paths ) {
            app.UseStaticFiles( );
            paths.Invoke( app );
            return app;
        }

        public static void IncludePath( this IApplicationBuilder app, string filesPath, string routePath ) {
            if ( !Directory.Exists( filesPath ) )
                Directory.CreateDirectory( filesPath );

            app.UseStaticFiles( new StaticFileOptions {
                FileProvider = new PhysicalFileProvider( filesPath ),
                RequestPath = $"/{routePath}"
            } );
        }
    }
}