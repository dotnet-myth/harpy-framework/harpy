﻿using Microsoft.Extensions.DependencyInjection;

namespace Harpy.Presentation.Extensions {

    public static class CorsExtension {

        public static IServiceCollection AddDefaultCorsPolice( this IServiceCollection services ) =>
            services.AddCors( options =>
                options.AddPolicy( "DefaultPolicy", builder => builder
                    .SetIsOriginAllowed( ( host ) => true )
                    .AllowAnyMethod( )
                    .AllowAnyHeader( )
                    .AllowCredentials( )
                )
            );
    }
}