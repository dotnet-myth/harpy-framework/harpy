﻿using Microsoft.Extensions.DependencyInjection;
using System.Globalization;

namespace Harpy.Presentation.Extensions {

    public static class CultureExtension {

        public static IServiceCollection AddCulture( this IServiceCollection services, string culture = "en-US" ) {
            var cultureInfo = new CultureInfo( culture );
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;
            return services;
        }
    }
}