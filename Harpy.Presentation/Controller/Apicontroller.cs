﻿using AutoMapper;
using Harpy.Domain.Interfaces.Messages;
using Harpy.Domain.Models.Errors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Harpy.Presentation.Controller {

    [Route( "api/[controller]" )]
    [ProducesResponseType( typeof( ExeceptionResponse ), StatusCodes.Status500InternalServerError )]
    [ProducesResponseType( typeof( ValidationResponse ), StatusCodes.Status422UnprocessableEntity )]
    public abstract class ApiController : ControllerBase {
        protected readonly IMessageBus _bus;

        protected readonly IMapper _mapper;

        protected ApiController(
                IMessageBus bus,
                IMapper mapper ) {
            _bus = bus;
            _mapper = mapper;
        }

        protected ApiController( ) {
        }

        protected new IActionResult Response( ) => Ok( );

        protected new IActionResult Response( object result ) => Ok( result );

        protected IActionResult ResponseRedirect( string actionName, object routeValues ) =>
            RedirectToAction( actionName, routeValues );

        protected IActionResult ResponseRedirect( string actionName, string controllerName, object routeValues ) =>
            RedirectToAction( actionName, controllerName, routeValues );
    }
}