﻿using Harpy.Presentation.ViewModels.Metrics.HealthCheck;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using NSwag.Annotations;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Presentation.Controller.Metrics {

    [Microsoft.AspNetCore.Mvc.Route( "api/Metrics" )]
    [OpenApiTags( "Metrics / Status" )]
    public class HealthController : ApiController {
        private readonly HealthCheckService _healthCheckService;

        public HealthController( HealthCheckService healthCheckService ) {
            _healthCheckService = healthCheckService;
        }

        [OpenApiOperation( "Get health status from server", "" )]
        [ProducesResponseType( typeof( HealthCheckViewModel ), StatusCodes.Status200OK )]
        [ProducesResponseType( typeof( HealthCheckViewModel ), StatusCodes.Status503ServiceUnavailable )]
        [HttpGet( "[controller]" )]
        public async Task<IActionResult> GetAsync( CancellationToken cancellationToken ) {
            var report = await _healthCheckService.CheckHealthAsync( cancellationToken );

            var response = new HealthCheckViewModel( ) {
                Status = report.Status.ToString( ),
                TotalDuration = report.TotalDuration.TotalSeconds + " seconds",
                Entries = report.Entries.Select( entry => new HealthCheckItemViewModel {
                    Name = entry.Key,
                    Status = entry.Value.Status.ToString( ),
                    Description = entry.Value.Description?.ToString( ),
                    Tags = entry.Value.Tags,
                    Duration = entry.Value.Duration.TotalSeconds.ToString( ) + " seconds",
                    Message = entry.Value.Exception?.Message
                } )
            };

            return report.Status == HealthStatus.Healthy
                ? Ok( response )
                : StatusCode( ( int )HttpStatusCode.ServiceUnavailable, response );
        }
    }
}