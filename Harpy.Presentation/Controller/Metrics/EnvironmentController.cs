﻿using Harpy.Presentation.ViewModels.Metrics.Environment;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using NSwag.Annotations;

namespace Harpy.Presentation.Controller.Metrics {

    [Microsoft.AspNetCore.Mvc.Route( "api/Metrics" )]
    [OpenApiTags( "Metrics / Status" )]
    public class EnvironmentController : ApiController {
        private readonly IHostEnvironment _environment;

        public EnvironmentController( IHostEnvironment environment ) {
            _environment = environment;
        }

        [OpenApiOperation( "Get current enviroment", "" )]
        [ProducesResponseType( typeof( EnvironmentViewModel ), StatusCodes.Status200OK )]
        [HttpGet( "[controller]" )]
        public IActionResult Get( ) {
            var response = new EnvironmentViewModel {
                EnvironmentName = _environment.EnvironmentName,
                IsDevelopment = _environment.IsDevelopment( ),
                IsStaging = _environment.IsStaging( ),
                IsProduction = _environment.IsProduction( ),
            };

            return Ok( response );
        }
    }
}