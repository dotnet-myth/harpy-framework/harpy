﻿using Harpy.Domain.Interfaces.Plugins;
using System.Collections.Generic;
using System.Reflection;

namespace Harpy.Plugin.Models {

    public class PluginControllers : List<Assembly>, IPluginControllers {

        public PluginControllers( ) : base( ) {
        }
    }
}