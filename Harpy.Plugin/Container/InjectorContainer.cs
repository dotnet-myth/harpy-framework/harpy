﻿using Harpy.Domain.Interfaces.Plugins;
using Harpy.IoC;
using Harpy.Plugin.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Harpy.Plugin.Container {

    public static class InjectorContainer {

        private static Assembly ResolveAssemblyMissing( object sender, ResolveEventArgs args ) {
            var requestedAssemblyName = args.Name;

            var currentLocal = Directory.GetParent( args.RequestingAssembly.Location );
            var pluginAssemblysOferted = currentLocal.GetFiles( "*.dll", SearchOption.AllDirectories );
            var assemblyPath = pluginAssemblysOferted.FirstOrDefault( x => requestedAssemblyName.Contains( Path.GetFileNameWithoutExtension( x.Name ) ) )?.FullName;

            if ( assemblyPath != default )
                return Assembly.LoadFrom( assemblyPath );

            var localAssemblies = AppDomain.CurrentDomain.GetAssemblies( );
            var assembly = localAssemblies.FirstOrDefault( x => x.FullName == requestedAssemblyName );

            if ( assembly != null )
                return assembly;

            throw new Exception( "A plugin's dependencies are missing!" );
        }

        public static IServiceCollection AddPlugins( this IServiceCollection services ) {
            var pluginsAssembly = new List<Assembly>( );
            var controllerAssembly = new PluginControllers( );

            var pluginsDirectory = Path.Combine( AppContext.BaseDirectory, "Plugins" );

            var pluginsInfo = new DirectoryInfo( pluginsDirectory );

            foreach ( var directory in pluginsInfo.GetDirectories( "*", SearchOption.TopDirectoryOnly ) ) {
                var dllsFinded = directory.GetFiles( "*.dll", SearchOption.TopDirectoryOnly );

                if ( dllsFinded.Any( ) ) {
                    AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler( ResolveAssemblyMissing );

                    var localAssemblies = new List<Assembly>( );

                    foreach ( var item in dllsFinded )
                        try {
                            localAssemblies.Add( Assembly.LoadFrom( item.FullName ) );
                        } catch ( Exception ) { }

                    pluginsAssembly.AddRange( localAssemblies.Where( x => !x.IsDynamic ).ToList( ) );

                    var plugin = pluginsAssembly.SelectMany( x => x.GetTypes( ) ).FirstOrDefault( t => typeof( IPluginServices ).IsAssignableFrom( t ) && !t.IsAbstract );

                    var containerService = ( IPluginServices )Activator.CreateInstance( plugin );
                    containerService.Register( services );

                    controllerAssembly.AddRange( containerService.ExportControllers( ) );
                }
            }

            services.AddCustomAssemblies( pluginsAssembly.ToArray( ) );

            services.AddSingleton<IPluginControllers>( controllerAssembly );

            return services;
        }
    }
}