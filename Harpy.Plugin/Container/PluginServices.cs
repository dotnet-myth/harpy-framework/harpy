﻿using Harpy.Domain.Interfaces.Plugins;
using Harpy.Plugin.Controller;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Harpy.Plugin.Container {

    public abstract class PluginServices : IPluginServices {

        public abstract void Register( IServiceCollection services );

        public virtual IEnumerable<Assembly> ExportControllers( ) {
            var types = AppDomain.CurrentDomain.GetAssemblies( )
                .Where( p => !p.IsDynamic )
                .Where( assembly => !( assembly.EntryPoint != null && assembly.EntryPoint.Name == "Main" && assembly.EntryPoint.ReturnType == typeof( void ) ) )
                .SelectMany( assembly => assembly.ExportedTypes )
                .ToList( );

            var pluginAssemplies = types
                .Where( type => type.IsSubclassOf( typeof( PluginController ) ) )
                .Select( type => type.Assembly )
                .ToList( );

            return pluginAssemplies;
        }
    }
}