
# Welcome to Harpy Framework

## What's Harpy?

<img src="logo.png" alt="Harpy Logo" align="right" width="110px"/>

Harpy is an accelerator framework written in .NET, for domain-based applications in a single solution. Applying industry standards for fast and secure application. The framework is divided into layers, contained in each service: [Domain](#domain-layer), [Application](#application-layer), [Cross Cutting](#crosscutting-layer), [Data](#data-layer), [Presentation](#presentation-layer), [Test](#test-layer).

For a good use of the framework it is necessary to have knowledge in the construction of domains and for it to be effective it is necessary to have a good knowledge of the business, for a modeling in which the domains reflect the business.

## Domain layer

The _**Domain layer**_ represents everything that this service can do.  This layer defines: [aggregates](/%2E%2E/wikis/aggregatemodel), [commands](/%2E%2E/wikis/command), [events](/%2E%2E/wikis/event), [jobs](/%2E%2E/wikis/job), [constants](/%2E%2E/wikis/constant), [extension methods](/%2E%2E/wikis/extension), [value objects](/%2E%2E/wikis/valueobject), [specifications](/%2E%2E/wikis/specification), [validations](/%2E%2E/wikis/command/validation) as well as the [interfaces](/%2E%2E/wikis/interfaces) to be used by the rest of the service. This layer must be inherited by all other projects of the same service. The main namespace used will be `Harpy.Domain`.

## Application layer

The _**Application layer**_ is where actions your service's actions will actually take place. In this layer, [command handlers](/%2E%2E/wikis/command/handler), [event handlers](/%2E%2E/wikis/event/handler), [job handlers](/%2E%2E/wikis/job/handler) and [queries](/%2E%2E/wikis/query) are defined. This layer must be inherited by the presentation layer and the cross cutting layer. The main namespace used will be `Harpy.Application`.

## CrossCutting layer

The _**Cross cutting layer**_ as the name says is the layer that cuts layers. It defines your [service container (IoC)](/%2E%2E/wikis/ioc), as well as calling service jobs that are independent. It makes the union between all the layers to be exposed to the presentation layer. It is necessary that it has the reference of the layers: domain, application and repositories, including those of anti corruption. The main namespace used will be Harpy.IoC.

## Data layer

The _**Data layer**_ is where communication with all types of data will be. It is defined: [contexts](/%2E%2E/wikis/context), [repositories](/%2E%2E/wikis/repository), [anti-corruption](/%2E%2E/wikis/repository/anticorruption) and [resources](/%2E%2E/wikis/resource). Communication with the database is done here, individually by service. The main namespaces used will be `Harpy.Context` and `Harpy.Repository`.

## Presentation layer

The _**Presentation layer**_ is the exposed part of the application. It will define: the [controllers](/%2E%2E/wikis/controller), the [viewmodels](/%2E%2E/wikis/viewmodel) and the entire configuration of the [services](/%2E%2E/wikis/startup). The main namespace used will be `Harpy.Presentation`.

## Test layer

The _**Test layer**_ is divided into two parts, the domain tests and the presentation tests. Domain tests are basically unit tests for each [command](/%2E%2E/wikis/unittest/command), [event](wiki/unittest/event), [job](wiki/unittest/job), [query](wiki/unittest/query). And the presentation test are functional tests, to test each [endpoint](wiki/functional/test). The main namespaces used will be `Harpy.Test.Domain` and `Harpy.Test.Presentation`.

## Libraries and packages

|       Base Packages       |                             Stable Package                              |                                                                     Pre-release Package                                                                                                                                            |
|---------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Harpy.Domain              | <a href="https://www.nuget.org/packages/harpy.domain"><img src="https://img.shields.io/nuget/v/harpy.domain"></a>                           | <a href="https://www.nuget.org/packages/harpy.domain/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.domain"></a>                            |
| Harpy.Application         | <a href="https://www.nuget.org/packages/harpy.application"><img src="https://img.shields.io/nuget/v/harpy.application"></a>                 | <a href="https://www.nuget.org/packages/harpy.application/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.application"></a>                  |
| Harpy.Bus                 | <a href="https://www.nuget.org/packages/harpy.bus"><img src="https://img.shields.io/nuget/v/harpy.bus"></a>                                 | <a href="https://www.nuget.org/packages/harpy.bus/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.bus"></a>                                  |
| Harpy.Context             | <a href="https://www.nuget.org/packages/harpy.context"><img src="https://img.shields.io/nuget/v/harpy.context"></a>                         | <a href="https://www.nuget.org/packages/harpy.context/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.context"></a>                          |
| Harpy.Repository          | <a href="https://www.nuget.org/packages/harpy.repository"><img src="https://img.shields.io/nuget/v/harpy.repository"></a>                   | <a href="https://www.nuget.org/packages/harpy.repository/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.repository"></a>                    |
| Harpy.IoC                 | <a href="https://www.nuget.org/packages/harpy.ioc"><img src="https://img.shields.io/nuget/v/harpy.ioc"></a>                                 | <a href="https://www.nuget.org/packages/harpy.ioc/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.ioc"></a>                                  |
| Harpy.Transaction         | <a href="https://www.nuget.org/packages/harpy.transaction"><img src="https://img.shields.io/nuget/v/harpy.transaction"></a>                 | <a href="https://www.nuget.org/packages/harpy.transaction/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.transaction"></a>                  |
| Harpy.Presentation        | <a href="https://www.nuget.org/packages/harpy.presentation"><img src="https://img.shields.io/nuget/v/harpy.presentation"></a>               | <a href="https://www.nuget.org/packages/harpy.presentation/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.presentation"></a>                |
| Harpy.Test.Domain         | <a href="https://www.nuget.org/packages/harpy.test.domain"><img src="https://img.shields.io/nuget/v/harpy.test.domain"></a>                 | <a href="https://www.nuget.org/packages/harpy.test.domain/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.test.domain"></a>                  |
| Harpy.Test.Presentation   | <a href="https://www.nuget.org/packages/harpy.test.presentation"><img src="https://img.shields.io/nuget/v/harpy.test.presentation"></a>     | <a href="https://www.nuget.org/packages/harpy.test.presentation/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.test.presentation"></a>      |

|     Database Packages     |                              Stable Package                             |                                                                     Pre-release Package                                                                                                                                            |
|---------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Harpy.SQLite          | <a href="https://www.nuget.org/packages/harpy.sqlite"><img src="https://img.shields.io/nuget/v/harpy.sqlite"></a>                   | <a href="https://www.nuget.org/packages/harpy.sqlite/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.sqlite"></a>                        |
| Harpy.Oracle          | <a href="https://www.nuget.org/packages/harpy.oracle"><img src="https://img.shields.io/nuget/v/harpy.oracle"></a>                   | <a href="https://www.nuget.org/packages/harpy.oracle/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.oracle"></a>                    |
| Harpy.LiteDB       | <a href="https://www.nuget.org/packages/harpy.litedb"><img src="https://img.shields.io/nuget/v/harpy.litedb"></a>             | <a href="https://www.nuget.org/packages/harpy.litedb/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.litedb"></a>              |
| Harpy.PostgreSQL          | <a href="https://www.nuget.org/packages/harpy.postgresql"><img src="https://img.shields.io/nuget/v/harpy.postgresql"></a>                   | <a href="https://www.nuget.org/packages/harpy.postgresql/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.postgresql"></a>                    |

|      Tools Packages       |                              Stable Package                             |                                                                     Pre-release Package                                                                                                                                            |
|---------------------------|-------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Harpy.CLI                 | <a href="https://www.nuget.org/packages/harpy.cli"><img src="https://img.shields.io/nuget/v/harpy.cli"></a>                                 | <a href="https://www.nuget.org/packages/harpy.cli/absoluteLatest"><img src="https://img.shields.io/nuget/vpre/harpy.cli"></a>                                  |


## Getting Started

1. Install Harpy at the command prompt if you haven't yet:

        $ dotnet tool install Harpy.Cli

2. At the command prompt, create a new Harpy application:

        $ harpy new myapp

   where `myapp`is the application name.

3. Change directory to `myapp` and cread a new service domain:

        $ cd myapp
        $ harpy domain new myservice

   where `myservice` is the service name.

4. Start the api server

        $ dotnet run

5. Go to `http://localhost:5000/swagger`
   and you will see the swagger with all the endpoints

<!-- 5. Follow the guidelines to start developing your application. You may find
   the following resources handy:
    * [Getting Started with Rails](https://guides.rubyonrails.org/getting_started.html)
    * [Ruby on Rails Guides](https://guides.rubyonrails.org)
    * [The API Documentation](https://api.rubyonrails.org)

## Contributing

[![Code Triage Badge](https://www.codetriage.com/rails/rails/badges/users.svg)](https://www.codetriage.com/rails/rails)

We encourage you to contribute to Ruby on Rails! Please check out the
[Contributing to Ruby on Rails guide](https://edgeguides.rubyonrails.org/contributing_to_ruby_on_rails.html) for guidelines about how to proceed. [Join us!](https://contributors.rubyonrails.org)

Trying to report a possible security vulnerability in Rails? Please
check out our [security policy](https://rubyonrails.org/security/) for
guidelines about how to proceed.

Everyone interacting in Rails and its sub-projects' codebases, issue trackers, chat rooms, and mailing lists is expected to follow the Rails [code of conduct](https://rubyonrails.org/conduct/).

## Code Status

[![Build Status](https://badge.buildkite.com/ab1152b6a1f6a61d3ea4ec5b3eece8d4c2b830998459c75352.svg?branch=master)](https://buildkite.com/rails/rails)
-->
## License

Harpy Framework is released under the [MIT License](https://opensource.org/licenses/MIT).
