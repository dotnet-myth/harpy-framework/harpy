﻿using FluentValidation;
using Myth.ValueObjects;
using System.Net;

namespace Harpy.Domain.Extensions {

    public static class FluentValidationExtensions {

        public static IRuleBuilderOptions<T, TProperty> WithMessageFormat<T, TProperty>( this IRuleBuilderOptions<T, TProperty> rule, string errorMessage, params object[ ] args ) =>
            rule.WithMessage( string.Format( errorMessage, args ) );

        public static IRuleBuilderOptions<T, TProperty> WithStatusCode<T, TProperty>( this IRuleBuilderOptions<T, TProperty> rule, HttpStatusCode statusCode ) =>
            rule.WithErrorCode( ( ( int )statusCode ).ToString( ) );
    }
}