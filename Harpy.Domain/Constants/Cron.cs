﻿using Myth.ValueObjects;

namespace Harpy.Domain.Constants {

    public class Cron : Constant<Cron, string> {
        public static readonly Cron Daily = new Cron( nameof( Daily ), Hangfire.Cron.Daily( ) );

        public static readonly Cron Hourly = new Cron( nameof( Hourly ), Hangfire.Cron.Hourly( ) );

        public static readonly Cron Minutely = new Cron( nameof( Minutely ), Hangfire.Cron.Minutely( ) );

        public static readonly Cron Monthly = new Cron( nameof( Monthly ), Hangfire.Cron.Monthly( ) );

        public static readonly Cron Weekly = new Cron( nameof( Weekly ), Hangfire.Cron.Weekly( ) );

        public static readonly Cron Yearly = new Cron( nameof( Yearly ), Hangfire.Cron.Yearly( ) );

        protected Cron( string name, string value )
            : base( name, value ) { }

        public Cron( int? minute = null, int? hour = null, int? day = null, int? month = null, int? dayOfWeek = null )
            : this( "Custom", ( minute.HasValue && minute >= 0 && minute <= 59 ? minute.ToString( ) : "*" ) + " " +
                    ( hour.HasValue && hour >= 0 && hour <= 23 ? hour.ToString( ) : "*" ) + " " +
                    ( day.HasValue && day >= 1 && day <= 31 ? day.ToString( ) : "*" ) + " " +
                    ( month.HasValue && month >= 1 && month <= 12 ? month.ToString( ) : "*" ) + " " +
                    ( dayOfWeek.HasValue && dayOfWeek >= 0 && dayOfWeek <= 6 ? dayOfWeek.ToString( ) : "*" ) ) {
        }
    }
}