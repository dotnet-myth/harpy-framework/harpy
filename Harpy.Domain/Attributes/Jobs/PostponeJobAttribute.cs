﻿using Hangfire.Common;
using Hangfire.States;
using Harpy.Domain.Exceptions.Jobs;

namespace Harpy.Domain.Attributes.Jobs {

    public class PostponeJobAttribute : JobFilterAttribute, IElectStateFilter {

        public void OnStateElection( ElectStateContext context ) {
            var failedState = context.CandidateState as FailedState;
            if ( failedState != null && failedState.Exception is PostponeJobException ) {
                var time = ( ( PostponeJobException )failedState.Exception ).PostponeTo;
                context.CandidateState = new ScheduledState( time );
            }
        }
    }
}