﻿using System.Collections.Generic;

namespace Harpy.Domain.Models.Errors {

    public class ExeceptionResponse {
        public string Message { get; set; }

        public string InnerMessage { get; set; }

        public IEnumerable<string> StackTrace { get; set; }

        public ExeceptionResponse( ) {
            StackTrace = new List<string>( );
        }

        public ExeceptionResponse( string message ) : this( ) {
            Message = message;
        }

        public ExeceptionResponse( string message, string stackTrace ) : this( message ) {
            StackTrace = stackTrace?.Split(
                new[ ] { "\r\n", "\n" },
                System.StringSplitOptions.RemoveEmptyEntries | System.StringSplitOptions.TrimEntries );
        }

        public ExeceptionResponse( string message, string stackTrace, string innerMessage ) : this( message, stackTrace ) {
            InnerMessage = innerMessage;
        }

        public override string ToString( ) =>
            $"\tMessage: {Message} \n\tInnerMessage: {InnerMessage} \n\tStackTrace: \n\t\t{string.Join( "\n\t\t", StackTrace )}";
    }
}