﻿using System.Collections.Generic;

namespace Harpy.Domain.Models.Errors {

    public class ValidationResponse {
        public IEnumerable<MessageResponse> Errors { get; set; }

        public ValidationResponse( ) {
            Errors = new List<MessageResponse>( );
        }

        public ValidationResponse( IEnumerable<MessageResponse> errors ) {
            Errors = errors;
        }
    }
}