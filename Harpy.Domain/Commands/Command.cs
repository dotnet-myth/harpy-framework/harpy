﻿using Harpy.Domain.Events.Base;
using Harpy.Domain.Interfaces.Definitions;

namespace Harpy.Domain.Commands {

    public class Command<TResponse> : Message<TResponse>, ICommand<TResponse>, ICommand {

        protected Command( ) {
        }
    }
}