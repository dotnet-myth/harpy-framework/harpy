﻿using System;

namespace Harpy.Domain.Exceptions.Jobs {

    public class PostponeJobException : Exception {
        public TimeSpan PostponeTo { get; private set; }

        public PostponeJobException( string message, TimeSpan postponeTo ) : base( message ) {
            PostponeTo = postponeTo;
        }
    }
}