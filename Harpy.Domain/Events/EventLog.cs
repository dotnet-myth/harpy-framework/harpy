﻿using Harpy.Domain.Events.Base;
using Harpy.Domain.ValueObjects;
using System;

namespace Harpy.Domain.Events {

    public class EventLog : Event {
        public string EventTypeName { get; private set; }

        public EventStateEnum State { get; set; }

        public string Content { get; private set; }

        public EventLog( ) {
        }

        public EventLog( Guid eventId, DateTime timestamp, string fullName, string content = default ) : base( eventId, timestamp ) {
            EventTypeName = fullName;
            Content = content;
            State = EventStateEnum.NotPublished;
        }

        public void MarkEventAsFailed( ) {
            State = EventStateEnum.PublishedFailed;
        }

        public void MarkEventAsPublished( ) {
            if ( State.HasFlag( EventStateEnum.NotPublished ) ) {
                State |= EventStateEnum.Published;
            }
        }

        public void MarkEventAsProcessed( ) {
            if ( State.HasFlag( EventStateEnum.Published ) ) {
                State |= EventStateEnum.Processed;
            }
        }
    }
}