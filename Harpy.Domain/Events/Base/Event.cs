﻿using Harpy.Domain.Interfaces.Definitions;
using System;

namespace Harpy.Domain.Events.Base {

    public abstract class Event : Message<bool>, IEvent {
        public Guid Id { get; private set; }

        public DateTime CreatedAt { get; private set; }

        protected Event( Guid id, DateTime createdAt ) {
            Id = id;
            CreatedAt = createdAt;
        }

        protected Event( Guid id ) {
            Id = id;
        }

        protected Event( ) {
            Id = Guid.NewGuid( );
            CreatedAt = DateTime.Now;
        }
    }
}