﻿using MediatR;

namespace Harpy.Domain.Events.Base {

    public class Message<TReseponse> : IRequest<TReseponse> {
        protected string MessageType { get; set; }

        protected Message( ) {
            MessageType = GetType( ).Name;
        }
    }
}