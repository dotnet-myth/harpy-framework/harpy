﻿using FluentValidation;

namespace Harpy.Domain.Interfaces.Validations {

    public interface IValidation : IValidator {
    }
}