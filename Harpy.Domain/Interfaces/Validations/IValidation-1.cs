﻿using FluentValidation;
using Harpy.Domain.Models.Errors;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Domain.Interfaces.Validations {

    public interface IValidation<T> : IValidator<T>, IValidation {

        Task EnsureIsValidAsync( T command, CancellationToken cancellationToken );

        Task<IEnumerable<MessageResponse>> GetErrosIfExistsAsync( T command, CancellationToken cancellationToken );

        void KeepValidatingOnError( );
    }
}