﻿using Hangfire;
using Harpy.Domain.Interfaces.Definitions;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Domain.Interfaces.Messages {

    public interface IMessageBus {

        Task<TResponse> SendCommandAsync<TResponse>( ICommand<TResponse> command, CancellationToken cancellationToken );

        Task RaiseEventAsync( IEvent @event, CancellationToken cancellationToken );

        string RaiseJob( IJob job, IJobCancellationToken jobCancellationToken = default );
    }
}