﻿using Myth.Interfaces.Repositories.Base;

namespace Harpy.Domain.Interfaces.Repositories.Base {

    public interface IAntiCorruptionLayerRepository : IRepository {
    }
}