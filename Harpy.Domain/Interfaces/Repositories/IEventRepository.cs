﻿using Harpy.Domain.Events;
using Harpy.Domain.Interfaces.Definitions;
using Myth.Interfaces.Repositories.EntityFramework;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Domain.Interfaces.Repositories {

    public interface IEventRepository : IReadWriteRepositoryAsync<EventLog> {

        Task AddAsync<T>( T theEvent, CancellationToken cancellationToken ) where T : IEvent;

        Task MarkEventAsPublishedAsync( Guid eventId, CancellationToken cancellationToken );

        Task MarkEventAsProcessedAsync( Guid eventId, CancellationToken cancellationToken );

        Task MarkEventAsFailedAsync( Guid eventId, CancellationToken cancellationToken );
    }
}