﻿using System;

namespace Harpy.Domain.Interfaces.Transactions {

    public interface ITransaction : IDisposable {

        void Commit( );
    }
}