﻿using System.Collections.Generic;
using System.Reflection;

namespace Harpy.Domain.Interfaces.Plugins {

    public interface IPluginControllers : IEnumerable<Assembly> {
    }
}