﻿using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Reflection;

namespace Harpy.Domain.Interfaces.Plugins {

    public interface IPluginServices {

        void Register( IServiceCollection services );

        IEnumerable<Assembly> ExportControllers( );
    }
}