﻿using MediatR;

namespace Harpy.Domain.Interfaces.Definitions {

    public interface ICommand<TResponse> : IRequest<TResponse> {
    }
}