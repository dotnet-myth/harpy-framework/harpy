﻿using MediatR;
using System;

namespace Harpy.Domain.Interfaces.Definitions {

    public interface IEvent : INotification {
        public Guid Id { get; }

        public DateTime CreatedAt { get; }
    }
}