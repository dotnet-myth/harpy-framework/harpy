﻿using MediatR;

namespace Harpy.Domain.Interfaces.Definitions {

    public interface IJob : INotification {
    }
}