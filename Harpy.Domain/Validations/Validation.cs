﻿using FluentValidation;
using Harpy.Domain.Interfaces.Validations;
using Harpy.Domain.Models.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Domain.Validations {

    public class Validation<T> : AbstractValidator<T>, IValidation<T>, IValidation {
        protected readonly HttpStatusCode DefaultErrorStatusCode;

        public Validation( ) {
            CascadeMode = CascadeMode.Stop;
            DefaultErrorStatusCode = HttpStatusCode.UnprocessableEntity;
        }

        public async Task<IEnumerable<MessageResponse>> GetErrosIfExistsAsync( T command, CancellationToken cancellationToken ) {
            var validations = new List<MessageResponse>( );

            var result = await ValidateAsync( command, cancellationToken );

            if ( !result.IsValid )
                validations = result?
                    .Errors?
                    .Select( error => new MessageResponse( error.PropertyName, error.ErrorMessage ) )
                    .ToList( );

            return validations;
        }

        private async Task<HttpStatusCode> GetErrorStatusCode( T command, CancellationToken cancellationToken ) {
            var result = await ValidateAsync( command, cancellationToken );

            var errorCode = result.Errors.FirstOrDefault( x => {
                var parseStatus = int.TryParse( x.ErrorCode, out var statusCode );
                return parseStatus &&
                    Enum.IsDefined( typeof( HttpStatusCode ), statusCode ) &&
                    statusCode != ( int )HttpStatusCode.UnprocessableEntity;
            } )?.ErrorCode;

            if ( errorCode is null )
                return DefaultErrorStatusCode;
            else
                return ( HttpStatusCode )int.Parse( errorCode );
        }

        public async Task EnsureIsValidAsync( T command, CancellationToken cancellationToken ) {
            var validationErrors = await GetErrosIfExistsAsync( command, cancellationToken );
            if ( validationErrors.Any( ) ) {
                var statusCode = await GetErrorStatusCode( command, cancellationToken );
                throw new Exceptions.ValidationException( validationErrors, "Error on validate the following properties!", statusCode );
            }
        }

        /// <summary>
        /// Changes to continue validating cascade rules even if failures happen.
        /// Default: Stop on first error
        /// </summary>
        public void KeepValidatingOnError( ) => CascadeMode = CascadeMode.Continue;
    }
}