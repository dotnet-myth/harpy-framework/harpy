﻿namespace Harpy.Domain.Jobs.Base {

    public abstract class ContinueJob : Job {
        public string ParentJob { get; private set; }

        public ContinueJob( string parentJob ) {
            ParentJob = parentJob;
        }
    }
}