﻿using Harpy.Domain.Events.Base;
using Harpy.Domain.Interfaces.Definitions;

namespace Harpy.Domain.Jobs.Base {

    public abstract class Job : Message<bool>, IJob {

        protected Job( ) {
        }
    }
}