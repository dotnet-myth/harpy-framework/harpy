﻿using MediatR;

namespace Harpy.Domain.Jobs.Base {

    public abstract class PersistedJob : Job, INotification {
        public string JobId { get; private set; }

        public string JobCron { get; private set; }

        public bool TriggerOnCreated { get; private set; }

        protected PersistedJob( string jobId, string jobCron ) {
            JobId = jobId;
            JobCron = jobCron;
            TriggerOnCreated = true;
        }

        public PersistedJob( string jobId, string jobCron, bool triggerOnCreated )
                        : this( jobId, jobCron ) {
            TriggerOnCreated = triggerOnCreated;
        }
    }
}