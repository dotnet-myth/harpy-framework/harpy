﻿using Harpy.Domain.Interfaces.Transactions;
using System.Transactions;

namespace Harpy.Transactions {

    public class Transaction : ITransaction {
        private readonly TransactionScope _transactionScope;

        public Transaction( ) {
            var transactionOptions = new TransactionOptions {
                IsolationLevel = IsolationLevel.ReadCommitted
            };

            _transactionScope = new TransactionScope(
                TransactionScopeOption.Required,
                transactionOptions,
                TransactionScopeAsyncFlowOption.Enabled );
        }

        public void Commit( ) => _transactionScope.Complete( );

        public void Dispose( ) => _transactionScope.Dispose( );
    }
}