﻿using Harpy.Domain.Events;
using Microsoft.EntityFrameworkCore;
using Myth.Contexts;

namespace Harpy.Context {

    public class EventStoreContext : BaseContext {
        public DbSet<EventLog> EventLog { get; set; }

        public EventStoreContext( DbContextOptions options, params object[ ] args ) : base( options ) {
        }
    }
}