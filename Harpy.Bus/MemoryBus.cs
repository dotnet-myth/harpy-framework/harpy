﻿using Hangfire;
using Harpy.Application.JobHandlers;
using Harpy.Domain.Interfaces.Definitions;
using Harpy.Domain.Interfaces.Messages;
using Harpy.Domain.Interfaces.Repositories;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Bus {

    public sealed class MemoryBus : MessageBus, IMessageBus {
        private readonly IMediator _mediator;

        public MemoryBus( IServiceProvider serviceProvider, IMediator mediator, IEventRepository eventRepository, ILogger<MemoryBus> logger ) : base( serviceProvider, eventRepository, logger ) {
            _mediator = mediator;
        }

        public override async Task<TResponse> SendCommandAsync<TResponse>( ICommand<TResponse> command, CancellationToken cancellationToken = default ) {
            await ValidateAsync( command, cancellationToken );

            _logger.LogInformation( "Running command `{0}`...", command.GetType( ).Name );

            return await _mediator.Send( command, cancellationToken );
        }

        public override async Task RaiseEventAsync( IEvent @event, CancellationToken cancellationToken = default ) {
            var eventRepositoryValid = _eventRepository is not null;
            try {
                await ValidateAsync( @event, cancellationToken );

                if ( eventRepositoryValid )
                    await _eventRepository.AddAsync( @event, cancellationToken );

                _logger.LogInformation( "Running event `{0}` with identifier `{1}`...", @event.GetType( ).Name, @event.Id );

                await _mediator.Publish( @event, cancellationToken );

                if ( eventRepositoryValid )
                    await _eventRepository.MarkEventAsPublishedAsync( @event.Id, cancellationToken );

                _logger.LogInformation( "Event `{0}` with identifier `{1}` executed successfully!", @event.GetType( ).Name, @event.Id );
            } catch ( Exception ) {
                if ( eventRepositoryValid )
                    await _eventRepository.MarkEventAsFailedAsync( @event.Id, cancellationToken );

                _logger.LogError( "Event `{0}` with identifier `{1}` executed with error!", @event.GetType( ).Name, @event.Id );

                throw;
            }
        }

        public override string RaiseJob( IJob job, IJobCancellationToken cancellationToken = default ) {
            ValidateAsync( job, CancellationToken.None ).GetAwaiter( ).GetResult( );

            var jobType = job.GetType( );
            var notificationType = typeof( INotificationHandler<> ).MakeGenericType( jobType );
            var instance = _serviceProvider.GetService( notificationType );
            var jobHandlerType = typeof( JobHandler<> ).MakeGenericType( jobType );
            var processMethod = jobHandlerType.GetMethod( "Process" );

            _logger.LogInformation( "Running job `{0}``...", job.GetType( ).Name );

            return ( string )processMethod.Invoke( instance, new object[ ] { job, cancellationToken != null ? cancellationToken.ShutdownToken : default } );
        }
    }
}