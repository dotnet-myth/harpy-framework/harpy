﻿using Hangfire;
using Harpy.Domain.Interfaces.Definitions;
using Harpy.Domain.Interfaces.Messages;
using Harpy.Domain.Interfaces.Repositories;
using Harpy.Domain.Interfaces.Validations;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Bus {

    public abstract class MessageBus : IMessageBus {
        protected readonly IServiceProvider _serviceProvider;
        protected readonly IEventRepository _eventRepository;
        protected readonly ILogger<MessageBus> _logger;

        public MessageBus( IServiceProvider serviceProvider, IEventRepository eventRepository, ILogger<MessageBus> logger ) {
            _eventRepository = eventRepository;
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        protected virtual async Task ValidateAsync<T>( T command, CancellationToken cancellationToken ) {
            var commandType = command.GetType( );
            var commandValidationType = typeof( IValidation<> ).MakeGenericType( commandType );
            var validationService = _serviceProvider.GetService( commandValidationType );

            if ( validationService is not null ) {
                _logger.LogInformation( "Running command validation `{0}`...", commandType.Name );

                var ensureIsValidMethod = commandValidationType.GetMethod( "EnsureIsValidAsync" );
                var result = ( Task )ensureIsValidMethod.Invoke( validationService, new object[ ] { command, cancellationToken } );
                await result;
            } else
                _logger.LogWarning( "The `{0}` command will be executed without validation, because it was not found!", commandType.Name );
        }

        public abstract Task RaiseEventAsync( IEvent @event, CancellationToken cancellationToken );

        public abstract string RaiseJob( IJob job, IJobCancellationToken jobCancellationToken = null );

        public abstract Task<TResponse> SendCommandAsync<TResponse>( ICommand<TResponse> command, CancellationToken cancellationToken );
    }
}