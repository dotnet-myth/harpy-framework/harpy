﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Harpy.Bus.Extensions {

    public static class TypeExtension {

        public static bool InheritsFrom( this Type type, Type heritageType, bool checkImplements = false ) {
            var list = new List<Type> { type };
            while ( type.BaseType != null ) {
                type = type.BaseType;
                list.Add( type );
            }

            if ( checkImplements ) {
                var interfaces = type.GetInterfaces( );
                while ( interfaces != null && interfaces.Any( ) ) {
                    list.AddRange( interfaces );
                    interfaces = interfaces.SelectMany( x => x.GetInterfaces( ) ).ToArray( );
                }
            }

            return list.Any( x => x == heritageType );
        }

        public static bool InheritsFrom( this Type type, bool checkImplements = false, params Type[ ] heritageType ) {
            var list = new List<Type> { type };
            while ( type.BaseType != null ) {
                type = type.BaseType;
                list.Add( type );
            }

            if ( checkImplements ) {
                var interfaces = type.GetInterfaces( );
                while ( interfaces != null && interfaces.Any( ) ) {
                    list.AddRange( interfaces );
                    interfaces = interfaces.SelectMany( x => x.GetInterfaces( ) ).ToArray( );
                }
            }

            return list.Any( x => heritageType.Any( y => x == y ) );
        }

        public static bool InheritsGenericFrom( this Type type, params Type[ ] heritageType ) {
            var genericTypes = type.GenericTypeArguments;
            var list = new List<Type>( genericTypes );
            foreach ( var genericType in genericTypes ) {
                while ( genericType.BaseType != null ) {
                    list.Add( genericType );
                }
            }

            return list.Any( x => heritageType.Any( y => x == y ) );
        }
    }
}