﻿using Myth.Rest;
using Xunit;
using Xunit.Abstractions;

namespace Harpy.Test.Presentation {

    public abstract class ScenarioBase<TStartup> : IClassFixture<TestFixture<TStartup>> where TStartup : class {
        protected readonly ITestOutputHelper _output;

        protected RestBuilder _client;

        public ScenarioBase( TestFixture<TStartup> fixture, ITestOutputHelper output ) {
            _client = fixture.ClientBuilder;
            _output = output;
        }
    }
}