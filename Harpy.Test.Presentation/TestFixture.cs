﻿using Harpy.Presentation.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Myth.Rest;
using System;
using System.IO;

namespace Harpy.Test.Presentation {

    public class TestFixture<TStartup> : IDisposable {
        private TestServer _server;
        private IServiceProvider _serviceProvider;
        private string _environment = "Development";

        public RestBuilder ClientBuilder;

        public void Dispose( ) {
            _server.Dispose( );
            ClientBuilder.Dispose( );
        }

        public virtual void ConfigureServices( IServiceCollection services ) {
        }

        protected virtual IWebHostBuilder TestWebHostBuilder( ) {
            var startupType = typeof( TStartup );

            var contentRoot = Path.GetDirectoryName( startupType.Assembly.Location );
            var appsettingsEnvironment = $"appsettings.{_environment}.json";

            var configurationBuilder = new ConfigurationBuilder( )
                .SetBasePath( contentRoot )
                .AddJsonFile( "appsettings.json", optional: false, reloadOnChange: true );

            if ( File.Exists( Path.Combine( contentRoot, appsettingsEnvironment ) ) )
                configurationBuilder.AddJsonFile( appsettingsEnvironment, optional: true, reloadOnChange: true );

            var host = new WebHostBuilder( )
                 .UseContentRoot( contentRoot )
                 .UseFullLog( )
                 .UseConfiguration( configurationBuilder.Build( ) )
                 .UseStartup( startupType )
                 .UseEnvironment( _environment )
                 .ConfigureServices( ConfigureServices );

            return host;
        }

        public void SetEnvironment( string environment = "Development" ) => _environment = environment;

        public TService GetService<TService>( ) => _serviceProvider.GetService<TService>( );

        public TestFixture( ) {
            // Create instance of test server
            _server = new TestServer( TestWebHostBuilder( ) );

            // Get the services from dependency injection in the server
            _serviceProvider = _server.Services;

            // Add configuration for client
            var baseClient = _server.CreateClient( );
            ClientBuilder = new RestBuilder( baseClient );
        }
    }
}