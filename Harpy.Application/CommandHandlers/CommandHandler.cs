﻿using Harpy.Application.Base;
using Harpy.Domain.Commands;
using Harpy.Domain.Interfaces.Messages;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Application.CommandHandlers {

    public abstract class CommandHandler<TCommand, TResponse> : Handler<TCommand>, IRequestHandler<TCommand, TResponse> where TCommand : Command<TResponse> {
        protected readonly IMessageBus _bus;

        protected CommandHandler( IMessageBus bus ) : base( ) {
            _bus = bus;
        }

        public abstract Task<TResponse> Handle( TCommand command, CancellationToken cancellationToken );
    }
}