﻿using Harpy.Domain.Interfaces.Handlers;

namespace Harpy.Application.Base {

    public abstract class Handler<TModel> : IHandler {

        protected Handler( ) {
        }
    }
}