﻿using Harpy.Application.Base;
using Harpy.Domain.Events.Base;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Application.EventHandlers {

    public abstract class EventHandler<TEvent> : Handler<TEvent>, INotificationHandler<TEvent> where TEvent : Event {

        protected EventHandler( ) : base( ) {
        }

        public abstract Task Handle( TEvent notification, CancellationToken cancellationToken = default );
    }
}