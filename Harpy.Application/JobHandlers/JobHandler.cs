﻿using Hangfire;
using Harpy.Application.Base;
using Harpy.Domain.Attributes.Jobs;
using Harpy.Domain.Jobs.Base;
using MediatR;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Application.JobHandlers {

    [PostponeJob]
    public abstract class JobHandler<TJob> : Handler<TJob>, INotificationHandler<TJob> where TJob : Job {

        protected JobHandler( ) : base( ) {
        }

        public abstract Task Handle( TJob notification, CancellationToken cancellationToken = default );

        public virtual string Process( TJob job, CancellationToken cancellationToken = default ) {
            Expression<Func<Task>> doHandle = ( ) => Handle( job, cancellationToken );
            switch ( job ) {
                case ContinueJob continueJob:
                    return BackgroundJob.ContinueJobWith( continueJob.ParentJob, doHandle, JobContinuationOptions.OnlyOnSucceededState );

                case PersistedJob persistedJob:
                    RecurringJob.AddOrUpdate( persistedJob.JobId, doHandle, persistedJob.JobCron );
                    if ( persistedJob.TriggerOnCreated )
                        RecurringJob.Trigger( persistedJob.JobId );
                    return persistedJob.JobId;

                default:
                    return BackgroundJob.Enqueue( doHandle );
            }
        }
    }
}