﻿using Harpy.Context;
using Harpy.Domain.Events;
using Harpy.Domain.Interfaces.Definitions;
using Harpy.Domain.Interfaces.Repositories;
using Myth.Repositories.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Repository {

    public class EventRepository : ReadWriteRepositoryAsync<EventLog>, IEventRepository {

        public EventRepository( EventStoreContext context )
            : base( context ) {
        }

        public async Task MarkEventAsFailedAsync( Guid eventId, CancellationToken cancellationToken ) {
            var @event = await FindAsync( cancellationToken, eventId );
            @event.MarkEventAsFailed( );

            await UpdateAsync( @event, cancellationToken );
            await SaveChangesAsync( cancellationToken );
        }

        public async Task MarkEventAsProcessedAsync( Guid eventId, CancellationToken cancellationToken ) {
            var @event = await FindAsync( cancellationToken, eventId );
            @event.MarkEventAsProcessed( );

            await UpdateAsync( @event, cancellationToken );
            await SaveChangesAsync( cancellationToken );
        }

        public async Task MarkEventAsPublishedAsync( Guid eventId, CancellationToken cancellationToken ) {
            var @event = await FindAsync( cancellationToken, eventId );
            @event.MarkEventAsPublished( );

            await UpdateAsync( @event, cancellationToken );
            await SaveChangesAsync( cancellationToken );
        }

        public async Task AddAsync<T>( T @event, CancellationToken cancellationToken ) where T : IEvent {
            var serializedData = JsonConvert.SerializeObject( @event );

            var storedEvent = new EventLog( @event.Id, @event.CreatedAt, @event.GetType( ).FullName, serializedData );

            await base.AddAsync( storedEvent, cancellationToken );
            await SaveChangesAsync( cancellationToken );
        }
    }
}