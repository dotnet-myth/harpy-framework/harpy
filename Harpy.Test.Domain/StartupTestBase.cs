﻿using Hangfire.MemoryStorage;
using Harpy.IoC;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Data;
using System.IO;
using System.Linq;

namespace Harpy.Test.Domain {

    public abstract class StartupTestBase {
        public readonly string _environment = "Development";

        public virtual void ConfigureServices( IServiceCollection services ) {
            services.AddHarpy( config => config.JobStorage = new MemoryStorage( ) );
        }

        public virtual void Configure( IServiceProvider provider ) {
        }

        public virtual void ConfigureHost( IHostBuilder hostBuilder ) =>
            hostBuilder.ConfigureServices( ConfigureServices );

        /// <summary>
        /// Fix for bug in xUnit, running "ConfigureServices" twice.
        /// Use to inject the required services only once.
        /// </summary>
        /// <param name="services">Service collection</param>
        /// <returns>bool</returns>
        public static bool IsFirstCall( IServiceCollection services ) =>
            !services.Any( x => x.ServiceType == typeof( IDbConnection ) );

        public virtual IConfigurationBuilder CreateConfigurationBuilder<TType>( ) {
            var contentRoot = Path.GetDirectoryName( typeof( TType ).Assembly.Location );
            var appsettingsEnvironment = $"appsettings.{_environment}.json";

            var configurationBuilder = new ConfigurationBuilder( )
                .SetBasePath( contentRoot )
                .AddJsonFile( "appsettings.json", optional: false, reloadOnChange: true );

            if ( File.Exists( Path.Combine( contentRoot!, appsettingsEnvironment ) ) )
                configurationBuilder.AddJsonFile( appsettingsEnvironment, optional: true, reloadOnChange: true );

            return configurationBuilder;
        }
    }
}