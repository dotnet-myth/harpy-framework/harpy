using Harpy.Domain.Interfaces.Messages;
using Harpy.Test.Domain.Mock.Messages;
using Myth.Contexts;
using System.Threading.Tasks;

namespace Harpy.Test.Domain.Commands {

    public class CommandTest {
        protected readonly BaseContext _context;

        protected readonly IMessageBus _messageBus;

        public CommandTest( BaseContext context ) {
            _context = context;

            _messageBus = MemoryBus.Mock( );
        }

        protected async Task CleanContextAsync( ) => await _context.Database.EnsureDeletedAsync( );

        protected async Task SaveContextAsync( ) => await _context.SaveChangesAsync( );
    }
}