﻿using Harpy.IoC.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Myth.Contexts;
using System;

namespace Harpy.Test.Domain.Extensions {

    public static class DatabaseExtension {

        public static IServiceCollection CreateTestDatabase<TContext>( this IServiceCollection services ) where TContext : BaseContext {
            var args = new Parameters( );
            var options = services.BuildServiceProvider( ).GetService<DbContextOptions>( );
            using var dbContext = ( TContext )Activator.CreateInstance( typeof( TContext ), options, args );
            dbContext.Database.EnsureCreated( );

            return services;
        }
    }
}