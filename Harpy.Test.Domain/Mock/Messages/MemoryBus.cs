﻿using Hangfire;
using Harpy.Domain.Events.Base;
using Harpy.Domain.Interfaces.Messages;
using Harpy.Domain.Jobs.Base;
using Moq;
using System.Threading;
using System.Threading.Tasks;

namespace Harpy.Test.Domain.Mock.Messages {

    public static class MemoryBus {

        public static IMessageBus Mock( ) {
            var mediatorHandler = new Mock<IMessageBus>( );
            mediatorHandler.Setup( x =>
                x.RaiseEventAsync(
                    It.IsAny<Event>( ),
                    It.IsAny<CancellationToken>( ) ) )
            .Returns( Task.FromResult<Event>( null ) );

            mediatorHandler.Setup( x =>
                x.RaiseJob(
                    It.IsAny<Job>( ),
                    It.IsAny<JobCancellationToken>( ) ) )
            .Returns( "" );

            return mediatorHandler.Object;
        }
    }
}